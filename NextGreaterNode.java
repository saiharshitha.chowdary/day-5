class Solution {
    public int[] nextLargerNodes(ListNode head) {
         if(head == null){ return null; }
        
        Map<Integer, Integer> map = new HashMap<>();
        Stack<Integer> stack = new Stack<>();
        List<Integer> list = new ArrayList<>();
        
        int idx = 0;                                // use variable 'idx' to keep track of each indexed position of returned array.
        
        while(head != null){
 
            while( !stack.isEmpty() && head.val > list.get(stack.peek()) ){   
                map.put(stack.pop(),head.val );     //store index of the current or previous value, and its the next greater node.
            }
            stack.push(idx);                        //save the index of the position into Stack.
            list.add(head.val);                     //store the int value corresponding to that indexed position.
            idx++;
            head = head.next;                       //move to next ListNode.
        }
        
        int[] res = new int[idx];
        for(int i :map.keySet()){    
            res[i] = map.get(i); 
        }
    
        return res;    
    }
}
        
    