class Solution {
    public int numComponents(ListNode head, int[] nums) {
        int m = nums.length;
        if(m == 0) return 0;
        
        boolean[] exists = new boolean[100001];
        for(int i=0; i<m; i++) exists[nums[i]] = true;
        
        int ans = 0;
        boolean prevPresent = false;
        while(head != null) {
            
            if(exists[head.val]){
                if(!prevPresent) ans++;
                prevPresent = true;
            }
            else prevPresent = false;
            head = head.next;
        }
        
        return ans;
    }
}
    